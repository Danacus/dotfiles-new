#!/usr/bin/env bash

function run {
  if ! pgrep -f $1 ;
  then
    $@&
  fi
}

run ~/.config/picom/launch.sh
#run ~/.config/awesome/autohidewibox.py ~/.config/awesome/autohidewibox.conf
run ~/Scripts/setbg.sh 
run dunst
run pulseeffects --gapplication-service
xsetwacom set "Wacom HID 50DB Finger touch" Gesture off
run touchegg
run nm-applet
run /usr/lib/kdeconnectd
run kdeconnect-indicator
run snixembed
#run music_wake.sh
