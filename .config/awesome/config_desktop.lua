return {
    table.unpack(require("config_common")),
    widgets = {
        top = {
            left = {

            },
            mid = {

            },
            right = {
                "volume",
                "clock"
            }
        },
        bottom = {
            left = {

            },
            mid = {
                --"mpd"
            },
            right = {
                "memory",
                "cpu",
                "thermal",
            }
        }
    },
    volume = {
        options = {"Master", "-D", "pulse"},
        sink = "@DEFAULT_SINK@"
    }
}
