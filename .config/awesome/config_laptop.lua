return {
    table.unpack(require("config_common")),
    widgets = {
        top = {
            left = {

            },
            mid = {

            },
            right = {
                "volume",
                "backlight",
                "wifi",
                "battery",
                "clock"
            }
        },
        bottom = {
            left = {

            },
            mid = {
                "mpd"
            },
            right = {
                "memory",
                "cpu",
                "thermal",
            }
        }
    },
    battery = "BAT1",
    net_interface = "wlp3s0",
    volume = {
        options = {"Master", "-D", "pulse"},
        sink = "@DEFAULT_SINK@"
    },
    thermal_zone = "thermal_zone0"
}
