local tonumber = tonumber
local string = { match = string.match }
local table  = { concat = table.concat }

local helpers = require("vicious.helpers")
local spawn = require("vicious.spawn")


-- xbacklight: provides backlight level
-- vicious.contrib.xbacklight
local xbacklight = {}

local function parse(stdout, stderr, exitreason, exitcode)
    return math.floor(tonumber(stdout))
end

function xbacklight.async(format, warg, callback)
    if not warg then return callback{} end
    if type(warg) ~= "table" then warg = { warg } end
    spawn.easy_async("xbacklight" .. table.concat(warg, " "),
                     function (...) callback(parse(...)) end)
end
-- }}}

return helpers.setasyncall(xbacklight)
