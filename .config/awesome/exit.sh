#!/bin/sh

ANSWER=$(echo -e "suspend\nlock\nlogout\nshutdown\nreboot" | rofi -dmenu)

case $ANSWER in
	"suspend") systemctl suspend;;
	"lock") xset s activate;;
    "logout") echo 'awesome.quit()' | awesome-client;;
	"shutdown") shutdown now;;
	"reboot") reboot;;
esac
