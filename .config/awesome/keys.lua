
-- Standard awesome library
local gears = require("gears")
local awful = require("awful")

local hotkeys_popup = require("awful.hotkeys_popup")
-- Enable hotkeys help widget for VIM and other apps
-- when client with a matching name is opened:
require("awful.hotkeys_popup.keys")

local Keys = {}

Keys.globalbuttons = gears.table.join(
    --awful.button({  }, 1, end_switcher)
)

-- {{{ Key bindings
Keys.globalkeys = gears.table.join(

    awful.key({}, "XF86AudioRaiseVolume", function () 
        awful.spawn("sh -c 'pactl set-sink-mute "..config.volume.sink.." false && pactl set-sink-volume "..config.volume.sink.." +5%'")
        gears.timer.start_new(0.2, function () vicious.force({ volumebox, }) end)
    end),
    awful.key({}, "XF86AudioLowerVolume", function () 
        awful.spawn("sh -c 'pactl set-sink-mute "..config.volume.sink.." false && pactl set-sink-volume "..config.volume.sink.." -5%'")
        gears.timer.start_new(0.2, function () vicious.force({ volumebox, }) end)
    end),
    awful.key({}, "XF86AudioMute", function () 
        awful.spawn("pactl set-sink-mute "..config.volume.sink.." toggle")
        gears.timer.start_new(0.2, function () vicious.force({ volumebox, }) end)
    end),

    awful.key({ "Control" }, "XF86MonBrightnessUp", function () 
        awful.spawn("xbacklight -inc 1")
    end),
    awful.key({ "Control" }, "XF86MonBrightnessDown", function () 
        awful.spawn("xbacklight -dec 1")
    end),
    awful.key({}, "XF86MonBrightnessUp", function () 
        awful.spawn("xbacklight -inc 5")
    end),
    awful.key({}, "XF86MonBrightnessDown", function () 
        awful.spawn("xbacklight -dec 5")
    end),

    --awful.key({ modkey }, "g", function () for s in screen do togglegaps(s, s.topwibox); s.dock_hide_timer:stop() end end),
    --awful.key({ modkey }, "g", toggle_switcher),
    awful.key({ modkey,           }, "s",      hotkeys_popup.show_help,
              {description="show help", group="awesome"}),
    awful.key({ modkey,           }, "Left",   awful.tag.viewprev,
              {description = "view previous", group = "tag"}),
    awful.key({ modkey,           }, "Right",  awful.tag.viewnext,
              {description = "view next", group = "tag"}),
    awful.key({ modkey,           }, "Escape", awful.tag.history.restore,
              {description = "go back", group = "tag"}),

    awful.key({ modkey,           }, "j",
        function ()
            awful.client.focus.byidx( 1)
        end,
        {description = "focus next by index", group = "client"}
    ),
    awful.key({ modkey,           }, "k",
        function ()
            awful.client.focus.byidx(-1)
        end,
        {description = "focus previous by index", group = "client"}
    ),
    awful.key({ modkey,           }, "w", function () dropdown:toggle() end,
              {description = "toggle dropdown terminal", group = "client"}),

    awful.key({          }, "F2", function () htop:toggle() end,
              {description = "toggle dropdown htop terminal", group = "client"}),

    awful.key({ modkey, "Shift"   }, "m", function () vimpc:toggle() end,
              {description = "toggle dropdown vimpc terminal", group = "client"}),

    awful.key({ modkey, "Shift"   }, "b", function () onboard:toggle() end,
              {description = "toggle onboard", group = "client"}),

    -- Layout manipulation
    awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx(  1)    end,
              {description = "swap with next client by index", group = "client"}),
    awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx( -1)    end,
              {description = "swap with previous client by index", group = "client"}),
    awful.key({ modkey, "Control" }, "j", function () awful.screen.focus_relative( 1) end,
              {description = "focus the next screen", group = "screen"}),
    awful.key({ modkey, "Control" }, "k", function () awful.screen.focus_relative(-1) end,
              {description = "focus the previous screen", group = "screen"}),
    awful.key({ modkey,           }, "u", awful.client.urgent.jumpto,
              {description = "jump to urgent client", group = "client"}),

    -- Standard program
    awful.key({ modkey,           }, "Return", function () awful.spawn(terminal) end,
              {description = "open a terminal", group = "launcher"}),
    awful.key({ modkey, "Control" }, "r", awesome.restart,
              {description = "reload awesome", group = "awesome"}),
    awful.key({ modkey, "Shift"   }, "e", function() awful.spawn("/home/daan/.config/awesome/exit.sh") end,
              {description = "quit awesome", group = "awesome"}),
    awful.key({ modkey,           }, "l",     function () awful.tag.incmwfact( 0.05)          end,
              {description = "increase master width factor", group = "layout"}),
    awful.key({ modkey,           }, "h",     function () awful.tag.incmwfact(-0.05)          end,
              {description = "decrease master width factor", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "h",     function () awful.tag.incnmaster( 1, nil, true) end,
              {description = "increase the number of master clients", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "l",     function () awful.tag.incnmaster(-1, nil, true) end,
              {description = "decrease the number of master clients", group = "layout"}),
    awful.key({ modkey, "Control" }, "h",     function () awful.tag.incncol( 1, nil, true)    end,
              {description = "increase the number of columns", group = "layout"}),
    awful.key({ modkey, "Control" }, "l",     function () awful.tag.incncol(-1, nil, true)    end,
              {description = "decrease the number of columns", group = "layout"}),
    awful.key({ modkey,           }, "space", function () 
        awful.layout.inc( 1); 
        layout_table[awful.screen.focused().selected_tag] = awful.screen.focused().selected_tag.layout
    end, {description = "select next", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "space", function () 
        awful.layout.inc(-1); 
        layout_table[awful.screen.focused().selected_tag] = awful.screen.focused().selected_tag.layout
    end, {description = "select previous", group = "layout"}),

    awful.key({ modkey }, "m", function() awful.spawn("mpc toggle") end),
    awful.key({ modkey }, "]", function() awful.spawn("mpc next") end),
    awful.key({ modkey }, "[", function() awful.spawn("mpc prev") end),

    awful.key({ modkey, "Control" }, "n",
              function ()
                  local c = awful.client.restore()
                  -- Focus restored client
                  if c then
                    c:emit_signal(
                        "request::activate", "key.unminimize", {raise = true}
                    )
                  end
              end,
              {description = "restore minimized", group = "client"}),

    -- Prompt
    awful.key({ modkey },            "r",     function () awful.screen.focused().mypromptbox:run() end,
              {description = "run prompt", group = "launcher"}),

    awful.key({ modkey }, "x",
              function ()
                  awful.prompt.run {
                    prompt       = "Run Lua code: ",
                    textbox      = awful.screen.focused().mypromptbox.widget,
                    exe_callback = awful.util.eval,
                    history_path = awful.util.get_cache_dir() .. "/history_eval"
                  }
              end,
              {description = "lua execute prompt", group = "awesome"}),
    -- Menubar
    awful.key({ modkey }, "p", function() game_mode_toggle() end,
              {description = "show the menubar", group = "launcher"}),
    awful.key({ modkey }, "d", function() rofi_spawn("rofi -show drun -me-select-entry '' -me-accept-entry 'MousePrimary'") end,
              {description = "launch rofi", group = "launcher"}),
    awful.key({ modkey, "Shift"   }, "g",
        function (c)
            rofi_spawn("dmenu_script lutris.sh")
        end ,
        {description = "Play a Game"})
)

Keys.clientkeys = gears.table.join(
    awful.key({ modkey,           }, "f",
        function (c)
            c.fullscreen = not c.fullscreen
            c:raise()
        end,
        {description = "toggle fullscreen", group = "client"}),
    awful.key({ modkey, "Shift"   }, "q",      function (c) c:kill()                         end,
              {description = "close", group = "client"}),
    awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle                     ,
              {description = "toggle floating", group = "client"}),
    awful.key({ modkey, "Control", "Shift" }, "space", function (c)  
        c.maximized = false 
        c.maximized_vertical = false
        c.maximized_horizontal = false
    end,
              {description = "toggle floating", group = "client"}),
    awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end,
              {description = "move to master", group = "client"}),
    awful.key({ modkey,           }, "o",      function (c) c:move_to_screen()               end,
              {description = "move to screen", group = "client"}),
    awful.key({ modkey,           }, "t",      function (c) c.ontop = not c.ontop            end,
              {description = "toggle keep on top", group = "client"}),
    awful.key({ modkey,           }, "n",
        function (c)
            -- The client currently has the input focus, so it cannot be
            -- minimized, since minimized clients can't have the focus.
            c.minimized = true
        end ,
        {description = "minimize", group = "client"})
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it work on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
    Keys.globalkeys = gears.table.join(Keys.globalkeys,
        -- View tag only.
        awful.key({ modkey }, "#" .. i + 9,
                  function ()
                        local screen = awful.screen.focused()
                        local tag = screen.tags[i]
                        if tag then
                           tag:view_only()
                        end
                  end,
                  {description = "view tag #"..i, group = "tag"}),
        -- Toggle tag display.
        awful.key({ modkey, "Control" }, "#" .. i + 9,
                  function ()
                      local screen = awful.screen.focused()
                      local tag = screen.tags[i]
                      if tag then
                         awful.tag.viewtoggle(tag)
                      end
                  end,
                  {description = "toggle tag #" .. i, group = "tag"}),
        -- Move client to tag.
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:move_to_tag(tag)
                          end
                     end
                  end,
                  {description = "move focused client to tag #"..i, group = "tag"}),
        -- Toggle tag on focused client.
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:toggle_tag(tag)
                          end
                      end
                  end,
                  {description = "toggle focused client on tag #" .. i, group = "tag"})
    )
end

Keys.clientbuttons = gears.table.join(
    awful.button({ }, 1, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
    end),
    awful.button({ }, 3, function (c)
    end),
    awful.button({ modkey }, 1, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
        awful.mouse.client.move(c)
    end),
    awful.button({ modkey }, 3, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
        awful.mouse.client.resize(c)
    end)
)

return Keys
