#!/bin/sh

get_status() {
    mpc status | tr '\n' '\r' | sed 's/.*\[\(.*\)\].*/\1/'
}

while true
do
    if [ $(get_status) == "playing" ]; then
        systemd-inhibit --who="Me, the one and only" --why="Because I say so" --what=handle-lid-switch sleep 10
        sleep 5
    fi
done


