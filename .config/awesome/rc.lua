-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.
pcall(require, "luarocks.loader")

-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")
-- Theme handling library
local beautiful = require("beautiful")
-- Notification library
local naughty = require("naughty")
local menubar = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup")
local vicious = require("vicious")
local timer = require("gears.timer")
local config = require("config_desktop")
local quake = require("quake")

-- Enable hotkeys help widget for VIM and other apps
-- when client with a matching name is opened:
require("awful.hotkeys_popup.keys")

local function round(x)
  if x%2 ~= 0.5 then
    return math.floor(x+0.5)
  end
  return x-0.5
end

local dropdown = quake:new({ 
    app = "alacritty",
    argname = "--class %s",
    border = 0, 
    height = 0.37
})

local htop = quake:new({ 
    app = "alacritty",
    name = "QuakeHtop",
    argname = "--class %s",
    extra = "-e htop",
    border = 0, 
    height = 0.13,
    width = 0.25
})

local vimpc = quake:new({ 
    app = "alacritty",
    name = "QuakeVimpc",
    argname = "--class %s",
    extra = "-e /home/daan/.cargo/bin/mpc-rs",
    border = 0,
    height = 0.25,
    width = 0.33,
    horiz = "center",
    vert = "bottom"
})

local onboard = quake:new({ 
    app = "onboard",
    name = "QuakeOnboard",
    argname = "--class %s",
    extra = "",
    border = 0, 
    height = 0.25,
    width = 1,
    horiz = "center",
    vert = "bottom"
})

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Oops, there were errors during startup!",
                     text = awesome.startup_errors })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({ preset = naughty.config.presets.critical,
                         title = "Oops, an error happened!",
                         text = tostring(err) })
        in_error = false
    end)
end
-- }}}

-- {{{ Variable definitions
-- Themes define colours, icons, font and wallpapers.
if not beautiful.init("/home/daan/.config/awesome/theme.lua") then
    print("Mr. Theme did an oopsie!")
end
beautiful.font = "Hack Nerd Font 9"

-- This is used later as the default terminal and editor to run.
terminal = "alacritty"
editor = os.getenv("EDITOR") or "vim"
editor_cmd = terminal .. " -e " .. editor

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = "Mod4"

-- Table of layouts to cover with awful.layout.inc, order matters.
awful.layout.layouts = {
    awful.layout.suit.spiral.dwindle,
    --awful.layout.suit.floating,
    awful.layout.suit.tile,
    --awful.layout.suit.tile.left
    awful.layout.suit.tile.bottom,
    awful.layout.suit.tile.top,
    awful.layout.suit.fair,
    --awful.layout.suit.fair.horizontal,
    --awful.layout.suit.spiral,
    --awful.layout.suit.max,
    --awful.layout.suit.max.fullscreen,
    --awful.layout.suit.magnifier,
    --awful.layout.suit.corner.nw,
    -- awful.layout.suit.corner.ne,
    -- awful.layout.suit.corner.sw,
    -- awful.layout.suit.corner.se,
}
-- }}}

awful.spawn.with_shell("/home/daan/.config/awesome/autorun.sh")



-- {{{ Menu
-- Create a launcher widget and a main menu
myawesomemenu = {
   { "hotkeys", function() hotkeys_popup.show_help(nil, awful.screen.focused()) end },
   { "manual", terminal .. " -e man awesome" },
   { "edit config", editor_cmd .. " " .. awesome.conffile },
   { "restart", awesome.restart },
   { "quit", function() awesome.quit() end },
}

mymainmenu = awful.menu({ items = { { "awesome", myawesomemenu, beautiful.awesome_icon },
                                    { "open terminal", terminal }
                                  }
                        })

mylauncher = awful.widget.launcher({ image = beautiful.awesome_icon,
                                     menu = mymainmenu })

-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it
-- }}}

-- Keyboard map indicator and switcher
mykeyboardlayout = awful.widget.keyboardlayout()

-- {{{ Wibar
-- Create a textclock widget
mytextclock = wibox.widget.textclock()

-- Create a wibox for each screen and add it
local taglist_buttons = gears.table.join(
                    awful.button({ }, 1, function(t) t:view_only() end),
                    awful.button({ modkey }, 1, function(t)
                                              if client.focus then
                                                  client.focus:move_to_tag(t)
                                              end
                                          end),
                    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ modkey }, 3, function(t)
                                              if client.focus then
                                                  client.focus:toggle_tag(t)
                                              end
                                          end),
                    awful.button({ }, 4, function(t) awful.tag.viewnext(t.screen) end),
                    awful.button({ }, 5, function(t) awful.tag.viewprev(t.screen) end)
                )

local tasklist_buttons = gears.table.join(
                     awful.button({ }, 1, function (c)
                                              if c == client.focus then
                                                  c.minimized = true
                                              else
                                                  c:emit_signal(
                                                      "request::activate",
                                                      "tasklist",
                                                      {raise = true}
                                                  )
                                              end
                                          end),
                     awful.button({ }, 3, function()
                                              awful.menu.client_list({ theme = { width = 250 } })
                                          end),
                     awful.button({ }, 4, function ()
                                              awful.client.focus.byidx(1)
                                          end),
                     awful.button({ }, 5, function ()
                                              awful.client.focus.byidx(-1)
                                          end))

local function set_wallpaper(s)
    -- Wallpaper
    if beautiful.wallpaper then
        local wallpaper = beautiful.wallpaper
        -- If wallpaper is a function, call it with the screen
        if type(wallpaper) == "function" then
            wallpaper = wallpaper(s)
        end
        gears.wallpaper.maximized(wallpaper, s, true)
    end
end


local backlight_widget_type = { 
    async = function (format, warg, callback)
        awful.spawn.easy_async("xbacklight ", function (stdout) 
            callback{ round(tonumber(stdout)) }
        end)
    end 
}

local battery_icon_table = {
    not_charging = {
        ["0"] = "",
        ["10"] = "",
        ["20"] = "",
        ["30"] = "",
        ["40"] = "",
        ["50"] = "",
        ["60"] = "",
        ["70"] = "",
        ["80"] = "",
        ["90"] = "",
        ["100"] = ""
    },
    charging = {
        ["0"] = "",
        ["10"] = "",
        ["20"] = "",
        ["30"] = "",
        ["40"] = "",
        ["50"] = "",
        ["60"] = "",
        ["70"] = "",
        ["80"] = "",
        ["90"] = "",
        ["100"] = ""
    },
}

local brightness_icon_table = {
    "", "", "", "", "", "", ""
}

local volume_icon_table = {
    levels = {
        "", "", ""
    },
    muted = "婢"
}

local thermal_icon_table = {
    "", "", "", "", ""
}

local function format_widget(...)
    local result = "  "
    for _, v in ipairs({...}) do
        result = result .. v .. "  "
    end
    return result
end

local function battery_icon(percent, charging)
    local key = ""..math.floor(round(percent / 10) * 10)..""
    if charging then
        return battery_icon_table.charging[key]
    else

        return battery_icon_table.not_charging[key]
    end
end

local function format_battery(widget, args)
    return format_widget(battery_icon(args[2], args[1] == "+"), args[2])
end

local function format_wifi(widget, args)
    if args["{ssid}"] ~= "N/A" then
        return format_widget("", args["{linp}"])
    end
    return ""
end

local function format_brightness(widget, args)
    local index = math.min(round(args[1] / 100 * 6 + 1), 7)
    return format_widget(brightness_icon_table[index], args[1])
end

local function format_volume(widget, args)
    if args[2] == "🔉" then
        local index = math.min(round(args[1] / 100 * 2 + 1), 3) 
        return format_widget(volume_icon_table.levels[index], args[1])
    else
        return format_widget(volume_icon_table.muted, "")
    end
end

local function format_memory(widget, args)
    return format_widget("", round(args[2] / 100) / 10  .. " GiB")
end

local function format_cpu(widget, args)
    return format_widget("", args[1])
end

local function format_mpd(widget, args)
    if args["{state}"] == "Stop" then
        return ''
    else
        local icon = (args["{state}"] == "Pause" and "" or "")
        return format_widget(icon, 
            ('%s - %s'):format(args["{Artist}"], args["{Title}"]), icon)
    end
end

local function format_thermal(widget, args)
    local key = math.floor(math.min(round(math.max(args[1] - 30, 0) / 60 * 4 + 1), 5))
    return format_widget(thermal_icon_table[key], args[1])
end


volumebox = wibox.widget.textbox()
vicious.cache(vicious.widgets.volume)
vicious.register(volumebox, vicious.widgets.volume, format_volume, 3, config.volume.options)

backlightbox = wibox.widget.textbox()
vicious.cache(backlight_widget_type)
vicious.register(backlightbox, backlight_widget_type, format_brightness, 3)

batwidget = wibox.widget.textbox()
vicious.cache(vicious.widgets.bat)
vicious.register(batwidget, vicious.widgets.bat, format_battery, 20, config.battery)

wifiwidget = wibox.widget.textbox()
vicious.cache(vicious.widgets.wifiiw)
vicious.register(wifiwidget, vicious.widgets.wifiiw, format_wifi, 3, config.net_interface)

cpuwidget = wibox.widget.textbox()
vicious.cache(vicious.widgets.cpu)
vicious.register(cpuwidget, vicious.widgets.cpu, format_cpu, 1)

memwidget = wibox.widget.textbox()
vicious.cache(vicious.widgets.mem)
vicious.register(memwidget, vicious.widgets.mem, format_memory, 1)

--mpdwidget = wibox.widget.textbox()
--mpdwidget.align = 'center'
--vicious.cache(vicious.widgets.mpd)
--vicious.register(mpdwidget, vicious.widgets.mpd, format_mpd)

thermalwidget = wibox.widget.textbox()
vicious.cache(vicious.widgets.thermal)
vicious.register(thermalwidget, vicious.widgets.thermal, format_thermal, 2, config.thermal_zone)

local widget_table = {
    --["mpd"] = mpdwidget,
    ["memory"] = memwidget,
    ["cpu"] = cpuwidget,
    ["volume"] = volumebox,
    ["backlight"] = backlightbox,
    ["wifi"] = wifiwidget,
    ["battery"] = batwidget,
    ["clock"] = mytextclock,
    ["thermal"] = thermalwidget,
}

local function load_widget_config(conf)
    local t = {}

    for _, v in ipairs(conf) do
        table.insert(t, widget_table[v])
    end

    return t
end

bar_timer = timer({ timeout = 0.2 })
bar_timer:connect_signal("timeout", function () disable_bars() end)
bar_mode = false

function enable_bars ()
    if bar_timer.started then bar_timer:stop() end
    if bar_mode == true then return else bar_mode = true end

    for s in screen do
        s.topwibox.visible = true
        s.bottomwibox.visible = true
    end
end

function disable_bars ()
    --[[
    if bar_timer.started then bar_timer:stop() end
    if bar_mode == false then return else bar_mode = false end

    for s in screen do
        s.topwibox.visible = false
        s.bottomwibox.visible = false
    end
    --]]
end


-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", set_wallpaper)


awful.screen.connect_for_each_screen(function(s)
    -- Wallpaper
    set_wallpaper(s)

    beautiful.useless_gap = 3

    -- Each screen has its own tag table.
    awful.tag({ "1", "2", "3", "4", "5", "6", "7", "8", "9" }, s, awful.layout.layouts[1])

    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(gears.table.join(
                           awful.button({ }, 1, function () awful.layout.inc( 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(-1) end),
                           awful.button({ }, 4, function () awful.layout.inc( 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(-1) end)))
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist {
        screen  = s,
        filter  = awful.widget.taglist.filter.all,
        buttons = taglist_buttons
    }

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist {
        screen  = s,
        filter  = awful.widget.tasklist.filter.currenttags,
        buttons = tasklist_buttons
    }

    -- Create the wibox
    s.topwibox = awful.wibar({ 
        position = "top", 
        screen = s, 
        bg = {
            type = "linear",
            from = { 0, 0, 0 },
            to = { 0, 20, 0 },
            stops = { { 0, "#000000aa" }, { 0.6, "#00000066"}, { 1, "#00000000" } }
        }, 
        visible = false 
    })

    -- Add widgets to the wibox
    s.topwibox:setup {
        layout = wibox.layout.align.horizontal,
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            s.mytaglist,
            
            table.unpack(load_widget_config(config.widgets.top.left))
        },
        {
            layout = wibox.layout.flex.horizontal,
            
            wibox.widget { widget = wibox.widget.separator, visible = false },

            table.unpack(load_widget_config(config.widgets.top.mid)),

            wibox.widget { widget = wibox.widget.separator, visible = false },
        },
        { -- Right widgets
            layout = wibox.layout.fixed.horizontal,

            table.unpack(load_widget_config(config.widgets.top.right)),
        },
    }

    s.bottomwibox = awful.wibar({ 
        position = "bottom", 
        screen = s, 
        bg = {
            type = "linear",
            from = { 0, 20, 0 },
            to = { 0, 0, 0 },
            stops = { { 0, "#000000aa" }, { 0.6, "#00000066"}, { 1, "#00000000" } }
        }, 
        visible = false 
    })

    s.bottomwibox:setup {
        layout = wibox.layout.align.horizontal,
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,

            wibox.widget.systray(),
            
            table.unpack(load_widget_config(config.widgets.bottom.left))
        },
        {
            layout = wibox.layout.flex.horizontal,
            
            table.unpack(load_widget_config(config.widgets.bottom.mid)),
        },
        { -- Right widgets
            layout = wibox.layout.fixed.horizontal,

            table.unpack(load_widget_config(config.widgets.bottom.right)),
        },
    }

    s.dock_trigger = wibox({ bg = "#00000000", opacity = 0, ontop = true, visible = true })
    s.dock_trigger:geometry({ width = 3, height = 3 })

    enable_bars()
    --s.topwibox:connect_signal("mouse::enter", function() enable_bars() end)
    --s.topwibox:connect_signal("mouse::leave", function() bar_timer:again() end)
    --s.bottomwibox:connect_signal("mouse::enter", function() enable_bars() end)
    --s.bottomwibox:connect_signal("mouse::leave", function() bar_timer:again() end)
    --s.dock_trigger:connect_signal("mouse::enter", function() enable_bars() end)
    --s.dock_trigger:connect_signal("mouse::leave", function() bar_timer:again() end)
end)
-- }}}

-- {{{ Mouse bindings
root.buttons(gears.table.join(
    awful.button({ }, 3, function () mymainmenu:toggle() end),
    awful.button({ }, 4, awful.tag.viewnext),
    awful.button({ }, 5, awful.tag.viewprev)
))
-- }}}

function table.copy(t)
  local u = { }
  for k, v in pairs(t) do u[k] = v end
  return setmetatable(u, getmetatable(t))
end


switcher_layout = awful.layout.suit.fair
switcher_mode = false
switcher_timer = timer({ timeout = 1 })
switcher_timer:connect_signal("timeout", function () end_switcher() end)
layout_table = {}
previous_tags = {}

function start_switcher ()
    if switcher_timer.started then switcher_timer:stop() end
    if switcher_mode == true then return else switcher_mode = true end

    beautiful.useless_gap = 10

    -- Store layout of all tags and set new layout
    for _, tag in ipairs(root.tags()) do
        layout_table[tag] = tag.layout
        tag.layout = switcher_layout
        tag.selected = true
    end

    for s in screen do
        for k,v in pairs(s.selected_tags) do table.insert(previous_tags, v) end
    end

    enable_bars()

    for _, c in ipairs(client.get()) do
        set_borders(c)
    end
end

function end_switcher ()
    if switcher_timer.started then switcher_timer:stop() end
    if switcher_mode == false then return else switcher_mode = false end

    beautiful.useless_gap = 3

    -- Restore layout of all tags
    for _, tag in ipairs(root.tags()) do
        tag.layout = layout_table[tag]
        if client.focus and tag ~= client.focus.first_tag or client.focus == nil then
            tag.selected = false
        end
    end
    -- Set the master window
    for _, c in ipairs(client.get()) do
        remove_borders(c)
    end

    -- Set focused client as master
    if client.focus then 
        client.focus:swap(awful.client.getmaster()) 
    else
        for _, tag in ipairs(previous_tags) do
            tag.selected = true
        end
    end

    disable_bars()

    previous_tags = {}
end

function toggle_switcher () 
    if switcher_mode then end_switcher() else start_switcher() end
end

function set_borders (c)
    c.border_width = beautiful.border_width
    awful.titlebar.show(c)
end

function remove_borders (c)
    c.border_width = 0
    awful.titlebar.hide(c)
end

rofi_pid = nil

function rofi_spawn (command) 
    rofi_kill()
    rofi_pid = awful.spawn(command)
end

function rofi_kill ()
    if rofi_pid then awful.spawn("kill "..rofi_pid) end
end

local game_mode = false

function game_mode_start ()
    if game_mode == true then return else game_mode = true end
    awful.spawn("killall picom")
    end_switcher()
    for s in screen do
        s.dock_trigger.visible = false
    end
end

function game_mode_stop ()
    if game_mode == false then return else game_mode = false end
    awful.spawn.with_shell("~/.config/picom/launch.sh")
    for s in screen do
        s.dock_trigger.visible = true
    end
end

function game_mode_toggle ()
    if game_mode then game_mode_stop() else game_mode_start() end
end


globalbuttons = gears.table.join(
    awful.button({  }, 1, end_switcher)
)

-- {{{ Key bindings
globalkeys = gears.table.join(

    awful.key({}, "XF86AudioRaiseVolume", function () 
        awful.spawn("sh -c 'pactl set-sink-mute "..config.volume.sink.." false && pactl set-sink-volume "..config.volume.sink.." +5%'")
        gears.timer.start_new(0.2, function () vicious.force({ volumebox, }) end)
    end),
    awful.key({}, "XF86AudioLowerVolume", function () 
        awful.spawn("sh -c 'pactl set-sink-mute "..config.volume.sink.." false && pactl set-sink-volume "..config.volume.sink.." -5%'")
        gears.timer.start_new(0.2, function () vicious.force({ volumebox, }) end)
    end),
    awful.key({}, "XF86AudioMute", function () 
        awful.spawn("pactl set-sink-mute "..config.volume.sink.." toggle")
        gears.timer.start_new(0.2, function () vicious.force({ volumebox, }) end)
    end),

    awful.key({ "Control" }, "XF86MonBrightnessUp", function () 
        awful.spawn("xbacklight -inc 1")
    end),
    awful.key({ "Control" }, "XF86MonBrightnessDown", function () 
        awful.spawn("xbacklight -dec 1")
    end),
    awful.key({}, "XF86MonBrightnessUp", function () 
        awful.spawn("xbacklight -inc 5")
    end),
    awful.key({}, "XF86MonBrightnessDown", function () 
        awful.spawn("xbacklight -dec 5")
    end),

    --awful.key({ modkey }, "g", function () for s in screen do togglegaps(s, s.topwibox); s.dock_hide_timer:stop() end end),
    awful.key({ modkey }, "g", toggle_switcher),
    awful.key({ modkey }, "Tab", function () start_switcher(); awful.client.focus.byidx(1); switcher_timer:again()  end),
    awful.key({ modkey,           }, "s",      hotkeys_popup.show_help,
              {description="show help", group="awesome"}),
    awful.key({ modkey,           }, "Left",   awful.tag.viewprev,
              {description = "view previous", group = "tag"}),
    awful.key({ modkey,           }, "Right",  awful.tag.viewnext,
              {description = "view next", group = "tag"}),
    awful.key({ modkey,           }, "Escape", awful.tag.history.restore,
              {description = "go back", group = "tag"}),

    awful.key({ modkey,           }, "j",
        function ()
            awful.client.focus.byidx( 1)
        end,
        {description = "focus next by index", group = "client"}
    ),
    awful.key({ modkey,           }, "k",
        function ()
            awful.client.focus.byidx(-1)
        end,
        {description = "focus previous by index", group = "client"}
    ),
    awful.key({ modkey,           }, "w", function () dropdown:toggle() end,
              {description = "toggle dropdown terminal", group = "client"}),

    awful.key({          }, "F2", function () htop:toggle() end,
              {description = "toggle dropdown htop terminal", group = "client"}),

    awful.key({ modkey, "Shift"   }, "m", function () vimpc:toggle() end,
              {description = "toggle dropdown vimpc terminal", group = "client"}),

    awful.key({ modkey, "Shift"   }, "b", function () onboard:toggle() end,
              {description = "toggle onboard", group = "client"}),

    -- Layout manipulation
    awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx(  1)    end,
              {description = "swap with next client by index", group = "client"}),
    awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx( -1)    end,
              {description = "swap with previous client by index", group = "client"}),
    awful.key({ modkey, "Control" }, "j", function () awful.screen.focus_relative( 1) end,
              {description = "focus the next screen", group = "screen"}),
    awful.key({ modkey, "Control" }, "k", function () awful.screen.focus_relative(-1) end,
              {description = "focus the previous screen", group = "screen"}),
    awful.key({ modkey,           }, "u", awful.client.urgent.jumpto,
              {description = "jump to urgent client", group = "client"}),

    -- Standard program
    awful.key({ modkey,           }, "Return", function () awful.spawn(terminal) end,
              {description = "open a terminal", group = "launcher"}),
    awful.key({ modkey, "Control" }, "r", awesome.restart,
              {description = "reload awesome", group = "awesome"}),
    awful.key({ modkey, "Shift"   }, "e", function() awful.spawn("/home/daan/.config/awesome/exit.sh") end,
              {description = "quit awesome", group = "awesome"}),
    awful.key({ modkey,           }, "l",     function () awful.tag.incmwfact( 0.05)          end,
              {description = "increase master width factor", group = "layout"}),
    awful.key({ modkey,           }, "h",     function () awful.tag.incmwfact(-0.05)          end,
              {description = "decrease master width factor", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "h",     function () awful.tag.incnmaster( 1, nil, true) end,
              {description = "increase the number of master clients", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "l",     function () awful.tag.incnmaster(-1, nil, true) end,
              {description = "decrease the number of master clients", group = "layout"}),
    awful.key({ modkey, "Control" }, "h",     function () awful.tag.incncol( 1, nil, true)    end,
              {description = "increase the number of columns", group = "layout"}),
    awful.key({ modkey, "Control" }, "l",     function () awful.tag.incncol(-1, nil, true)    end,
              {description = "decrease the number of columns", group = "layout"}),
    awful.key({ modkey,           }, "space", function () 
        --start_switcher(); 
        awful.layout.inc( 1); 
        --layout_table[awful.screen.focused().selected_tag] = awful.screen.focused().selected_tag.layout
        --switcher_timer:again() 
    end, {description = "select next", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "space", function () 
        start_switcher(); 
        awful.layout.inc(-1); 
        layout_table[awful.screen.focused().selected_tag] = awful.screen.focused().selected_tag.layout
        switcher_timer:again() 
    end, {description = "select previous", group = "layout"}),

    awful.key({ modkey }, "m", function() awful.spawn("mpc toggle") end),
    awful.key({ modkey }, "]", function() awful.spawn("mpc next") end),
    awful.key({ modkey }, "[", function() awful.spawn("mpc prev") end),

    awful.key({ modkey, "Control" }, "n",
              function ()
                  local c = awful.client.restore()
                  -- Focus restored client
                  if c then
                    c:emit_signal(
                        "request::activate", "key.unminimize", {raise = true}
                    )
                  end
              end,
              {description = "restore minimized", group = "client"}),

    -- Prompt
    awful.key({ modkey },            "r",     function () awful.screen.focused().mypromptbox:run() end,
              {description = "run prompt", group = "launcher"}),

    awful.key({ modkey }, "x",
              function ()
                  awful.prompt.run {
                    prompt       = "Run Lua code: ",
                    textbox      = awful.screen.focused().mypromptbox.widget,
                    exe_callback = awful.util.eval,
                    history_path = awful.util.get_cache_dir() .. "/history_eval"
                  }
              end,
              {description = "lua execute prompt", group = "awesome"}),
    -- Menubar
    awful.key({ modkey }, "p", function() game_mode_toggle() end,
              {description = "show the menubar", group = "launcher"}),
    awful.key({ modkey }, "d", function() rofi_spawn("rofi -show drun -me-select-entry '' -me-accept-entry 'MousePrimary'") end,
              {description = "launch rofi", group = "launcher"}),
    awful.key({ modkey }, "c", function() rofi_spawn("rofi-pass") end,
              {description = "launch rofi-pass", group = "launcher"}),
    awful.key({ modkey, "Shift"   }, "g",
        function (c)
            rofi_spawn("dmenu_script lutris.sh")
        end ,
        {description = "Play a Game"})
)

clientkeys = gears.table.join(
    awful.key({ modkey,           }, "f",
        function (c)
            c.fullscreen = not c.fullscreen
            c:raise()
        end,
        {description = "toggle fullscreen", group = "client"}),
    awful.key({ modkey, "Shift"   }, "q",      function (c) c:kill()                         end,
              {description = "close", group = "client"}),
    awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle                     ,
              {description = "toggle floating", group = "client"}),
    awful.key({ modkey, "Control", "Shift" }, "space", function (c)  
        c.maximized = false 
        c.maximized_vertical = false
        c.maximized_horizontal = false
    end,
              {description = "toggle floating", group = "client"}),
    awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end,
              {description = "move to master", group = "client"}),
    awful.key({ modkey,           }, "o",      function (c) c:move_to_screen()               end,
              {description = "move to screen", group = "client"}),
    awful.key({ modkey,           }, "t",      function (c) c.ontop = not c.ontop            end,
              {description = "toggle keep on top", group = "client"}),
    awful.key({ modkey,           }, "n",
        function (c)
            -- The client currently has the input focus, so it cannot be
            -- minimized, since minimized clients can't have the focus.
            c.minimized = true
        end ,
        {description = "minimize", group = "client"})
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it work on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
    globalkeys = gears.table.join(globalkeys,
        -- View tag only.
        awful.key({ modkey }, "#" .. i + 9,
                  function ()
                        local screen = awful.screen.focused()
                        local tag = screen.tags[i]
                        if tag then
                           tag:view_only()
                        end

                        if switcher_mode and switcher_timer.started then
                           switcher_timer:again() 
                        end
                  end,
                  {description = "view tag #"..i, group = "tag"}),
        -- Toggle tag display.
        awful.key({ modkey, "Control" }, "#" .. i + 9,
                  function ()
                      local screen = awful.screen.focused()
                      local tag = screen.tags[i]
                      if tag then
                         awful.tag.viewtoggle(tag)
                      end
                  end,
                  {description = "toggle tag #" .. i, group = "tag"}),
        -- Move client to tag.
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:move_to_tag(tag)
                          end
                     end
                  end,
                  {description = "move focused client to tag #"..i, group = "tag"}),
        -- Toggle tag on focused client.
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:toggle_tag(tag)
                          end
                      end
                  end,
                  {description = "toggle focused client on tag #" .. i, group = "tag"})
    )
end

clientbuttons = gears.table.join(
    awful.button({ }, 1, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
        end_switcher()
    end),
    awful.button({ }, 3, function (c)
        if switcher_mode then
            c:kill()
        end
    end),
    awful.button({ modkey }, 1, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
        awful.mouse.client.move(c)
    end),
    awful.button({ modkey }, 3, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
        awful.mouse.client.resize(c)
    end)
)

-- Set keys
root.keys(globalkeys)
root.buttons(globalbuttons)
-- }}}

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = awful.client.focus.filter,
                     raise = true,
                     keys = clientkeys,
                     buttons = clientbuttons,
                     screen = awful.screen.preferred,
                     placement = awful.placement.no_overlap+awful.placement.no_offscreen,
                     maximized_vertical   = false,
                     maximized_horizontal = false,
                     maximized = false
     }
    },

    -- Floating clients.
    { rule_any = {
        instance = {
          "DTA",  -- Firefox addon DownThemAll.
          "copyq",  -- Includes session name in class.
          "pinentry",
        },
        class = {
          "Arandr",
          "Blueman-manager",
          "Gpick",
          "Kruler",
          "MessageWin",  -- kalarm.
          "Sxiv",
          "Tor Browser", -- Needs a fixed window size to avoid fingerprinting by screen size.
          "Wpa_gui",
          "veromix",
          "xtightvncviewer",
          "tilda",
          "Tilda"
        },

        -- Note that the name property shown in xprop might be set slightly after creation of the client
        -- and the name shown there might not match defined rules here.
        name = {
          "Event Tester",  -- xev.
        },
        role = {
          "AlarmWindow",  -- Thunderbird's calendar.
          "ConfigManager",  -- Thunderbird's about:config.
          "pop-up",       -- e.g. Google Chrome's (detached) Developer Tools.
        }
      }, properties = { floating = true }},
      
    { rule_any = {
        name = {
          "Onboard", 
        },
        class = {
            "onboard", "Onboard"
        }
      }, properties = { focusable = false }},
      

    -- Add titlebars to normal clients and dialogs
    { rule_any = {type = { "normal", "dialog" }
      }, properties = { titlebars_enabled = false }
    },

    -- Set Firefox to always map on the tag named "2" on screen 1.
    -- { rule = { class = "Firefox" },
    --   properties = { screen = 1, tag = "2" } },
}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c)
    if switcher_mode then set_borders(c) else remove_borders(c) end
    -- Set the windows at the slave,
    -- i.e. put it at the end of others instead of setting it master.
    -- if not awesome.startup then awful.client.setslave(c) end

    if awesome.startup
      and not c.size_hints.user_position
      and not c.size_hints.program_position then
        -- Prevent clients from being unreachable after screen count changes.
        awful.placement.no_offscreen(c)
    end
end)

-- Add a titlebar if titlebars_enabled is set to true in the rules.
client.connect_signal("request::titlebars", function(c)
    -- buttons for the titlebar
    local buttons = gears.table.join(
        awful.button({ }, 1, function()
            c:emit_signal("request::activate", "titlebar", {raise = true})
            awful.mouse.client.move(c)
        end),
        awful.button({ }, 3, function()
            c:emit_signal("request::activate", "titlebar", {raise = true})
            awful.mouse.client.resize(c)
        end)
    )

    awful.titlebar(c, {
        bg_normal = beautiful.border_normal
    }) : setup {
        { -- Left
            awful.titlebar.widget.iconwidget(c),
            buttons = buttons,
            layout  = wibox.layout.fixed.horizontal
        },
        { -- Middle
            { -- Title
                align  = "center",
                widget = awful.titlebar.widget.titlewidget(c)
            },
            buttons = buttons,
            layout  = wibox.layout.flex.horizontal
        },
        { -- Right
            layout = wibox.layout.fixed.horizontal()
        },
        layout = wibox.layout.align.horizontal
    }

end)


client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
    c:emit_signal("request::activate", "mouse_enter", {raise = false})
end)


function dump(o)
   if type(o) == 'table' then
      local s = ''
      for k,v in pairs(o) do
         if type(k) ~= 'number' then k = '"'..k..'"' end
         s = s .. dump(v) .. ','
      end
      return s:sub(0, -2)
   else
      return tostring(type(o) == "tag" and o.name or o)
   end
end

function get_tag()
    return awful.screen.focused().selected_tag.name
end

function get_tags()
    return dump(awful.screen.focused().tags)
end


-- }}}
