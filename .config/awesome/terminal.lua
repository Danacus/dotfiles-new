local quake = require("quake")

local Terminal = {
    dropdown = quake:new({ 
        app = "termite",
        argname = "--name %s",
        border = 0, 
        height = 0.37
    }),
    htop = quake:new({ 
        app = "termite",
        name = "QuakeHtop",
        argname = "--name %s",
        extra = "-e htop",
        border = 0, 
        height = 0.13,
        width = 0.25
    }),
    vimpc = quake:new({ 
        app = "termite",
        name = "QuakeVimpc",
        argname = "--name %s",
        extra = "-e vimpc",
        border = 0, 
        height = 0.25,
        width = 0.33,
        horiz = "center",
        vert = "bottom"
    }),
    onboard = quake:new({ 
        app = "onboard",
        name = "QuakeOnboard",
        argname = "--name %s",
        extra = "",
        border = 0, 
        height = 0.25,
        width = 1,
        horiz = "center",
        vert = "bottom"
    })
}
