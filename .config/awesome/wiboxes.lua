
-- Standard awesome library
local gears = require("gears")
local awful = require("awful")

-- Widget and layout library
local wibox = require("wibox")
local menubar = require("menubar")

-- Load the config file
local config = require("config")

local widgets = require("widgets")

-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it
-- }}}

-- Keyboard map indicator and switcher
mykeyboardlayout = awful.widget.keyboardlayout()

-- {{{ Wibar
-- Create a textclock widget
mytextclock = wibox.widget.textclock()

-- Create a wibox for each screen and add it
local taglist_buttons = gears.table.join(
                    awful.button({ }, 1, function(t) t:view_only() end),
                    awful.button({ modkey }, 1, function(t)
                                              if client.focus then
                                                  client.focus:move_to_tag(t)
                                              end
                                          end),
                    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ modkey }, 3, function(t)
                                              if client.focus then
                                                  client.focus:toggle_tag(t)
                                              end
                                          end),
                    awful.button({ }, 4, function(t) awful.tag.viewnext(t.screen) end),
                    awful.button({ }, 5, function(t) awful.tag.viewprev(t.screen) end)
                )

local tasklist_buttons = gears.table.join(
                     awful.button({ }, 1, function (c)
                                              if c == client.focus then
                                                  c.minimized = true
                                              else
                                                  c:emit_signal(
                                                      "request::activate",
                                                      "tasklist",
                                                      {raise = true}
                                                  )
                                              end
                                          end),
                     awful.button({ }, 3, function()
                                              awful.menu.client_list({ theme = { width = 250 } })
                                          end),
                     awful.button({ }, 4, function ()
                                              awful.client.focus.byidx(1)
                                          end),
                     awful.button({ }, 5, function ()
                                              awful.client.focus.byidx(-1)
                                          end))

local Wiboxes = {}

function Wiboxes.connect_screen(s)
    -- Each screen has its own tag table.
    awful.tag({ "1", "2", "3", "4", "5", "6", "7", "8", "9" }, s, awful.layout.layouts[1])

    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(gears.table.join(
                           awful.button({ }, 1, function () awful.layout.inc( 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(-1) end),
                           awful.button({ }, 4, function () awful.layout.inc( 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(-1) end)))
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist {
        screen  = s,
        filter  = awful.widget.taglist.filter.all,
        buttons = taglist_buttons
    }

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist {
        screen  = s,
        filter  = awful.widget.tasklist.filter.currenttags,
        buttons = tasklist_buttons
    }

    -- Create the wibox
    s.topwibox = awful.wibar({ 
        position = "top", 
        screen = s, 
        bg = {
            type = "linear",
            from = { 0, 0, 0 },
            to = { 0, 20, 0 },
            stops = { { 0, "#000000aa" }, { 0.6, "#00000066"}, { 1, "#00000000" } }
        }, 
        visible = false 
    })

    -- Add widgets to the wibox
    s.topwibox:setup {
        layout = wibox.layout.align.horizontal,
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            s.mytaglist,
            
            table.unpack(widgets.get(config.widgets.top.left))
        },
        {
            layout = wibox.layout.flex.horizontal,
            
            wibox.widget { widget = wibox.widget.separator, visible = false },

            table.unpack(widgets.get(config.widgets.top.mid)),

            wibox.widget { widget = wibox.widget.separator, visible = false },
        },
        { -- Right widgets
            layout = wibox.layout.fixed.horizontal,

            table.unpack(widgets.get(config.widgets.top.right)),
        },
    }

    s.bottomwibox = awful.wibar({ 
        position = "bottom", 
        screen = s, 
        bg = {
            type = "linear",
            from = { 0, 20, 0 },
            to = { 0, 0, 0 },
            stops = { { 0, "#000000aa" }, { 0.6, "#00000066"}, { 1, "#00000000" } }
        }, 
        visible = false 
    })

    s.bottomwibox:setup {
        layout = wibox.layout.align.horizontal,
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,

            wibox.widget.systray(),
            
            table.unpack(widgets.get(config.widgets.bottom.left))
        },
        {
            layout = wibox.layout.flex.horizontal,
            
            table.unpack(widgets.get(config.widgets.bottom.mid)),
        },
        { -- Right widgets
            layout = wibox.layout.fixed.horizontal,

            table.unpack(widgets.get(config.widgets.bottom.right)),
        },
    }

    s.dock_trigger = wibox({ bg = "#00000000", opacity = 0, ontop = true, visible = true })
    s.dock_trigger:geometry({ width = 100, height = 1 })

    s.topwibox:connect_signal("mouse::enter", function() enable_bars() end)
    s.topwibox:connect_signal("mouse::leave", function() bar_timer:again() end)
    s.bottomwibox:connect_signal("mouse::enter", function() enable_bars() end)
    s.bottomwibox:connect_signal("mouse::leave", function() bar_timer:again() end)
    s.dock_trigger:connect_signal("mouse::enter", function() enable_bars() end)
    s.dock_trigger:connect_signal("mouse::leave", function() bar_timer:again() end)
end

bar_timer = timer({ timeout = 0.2 })
bar_timer:connect_signal("timeout", function () disable_bars() end)
bar_mode = false

function enable_bars ()
    if bar_timer.started then bar_timer:stop() end
    if bar_mode == true then return else bar_mode = true end

    for s in screen do
        s.topwibox.visible = true
        s.bottomwibox.visible = true
    end
end

function disable_bars ()
    if bar_timer.started then bar_timer:stop() end
    if bar_mode == false then return else bar_mode = false end

    for s in screen do
        s.topwibox.visible = false
        s.bottomwibox.visible = false
    end
end

return Wiboxes
