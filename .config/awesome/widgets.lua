
-- Standard awesome library
local gears = require("gears")
local awful = require("awful")

-- Widget and layout library
local wibox = require("wibox")

local icons = require("icons")
local config = require("config")
local vicious = require("vicious")

local util = require("util")

local backlight_widget_type = { 
    async = function (format, warg, callback)
        awful.spawn.easy_async("xbacklight ", function (stdout) 
            callback{ round(tonumber(stdout)) }
        end)
    end 
}

local function format_widget(...)
    local result = "  "
    for _, v in ipairs({...}) do
        result = result .. v .. "  "
    end
    return result
end

local function battery_icon(percent, charging)
    local key = ""..math.floor(util.round(percent / 10) * 10)..""
    if charging then
        return icons.battery.charging[key]
    else

        return icons.battery.not_charging[key]
    end
end

local function format_battery(widget, args)
    return format_widget(battery_icon(args[2], args[1] == "+"), args[2])
end

local function format_wifi(widget, args)
    if args["{ssid}"] ~= "N/A" then
        return format_widget("", args["{linp}"])
    end
    return ""
end

local function format_brightness(widget, args)
    local index = math.min(util.round(args[1] / 100 * 6 + 1), 7)
    return format_widget(icons.brightness[index], args[1])
end

local function format_volume(widget, args)
    if args[2] == "🔉" then
        local index = math.min(util.round(args[1] / 100 * 2 + 1), 3) 
        return format_widget(icons.volume.levels[index], args[1])
    else
        return format_widget(icons.volume.muted, "")
    end
end

local function format_memory(widget, args)
    return format_widget("", util.round(args[2] / 100) / 10  .. " GiB")
end

local function format_cpu(widget, args)
    return format_widget("", args[1])
end

local function format_mpd(widget, args)
    if args["{state}"] == "Stop" then
        return ''
    else
        local icon = (args["{state}"] == "Pause" and "" or "")
        return format_widget(icon, 
            ('%s - %s'):format(args["{Artist}"], args["{Title}"]), icon)
    end
end

local function format_thermal(widget, args)
    local key = math.floor(math.min(util.round(math.max(args[1] - 30, 0) / 60 * 4 + 1), 5))
    return format_widget(icons.thermal[key], args[1])
end

volumebox = wibox.widget.textbox()
vicious.cache(vicious.widgets.volume)
vicious.register(volumebox, vicious.widgets.volume, format_volume, 3, config.volume.options)

backlightbox = wibox.widget.textbox()
vicious.cache(backlight_widget_type)
vicious.register(backlightbox, backlight_widget_type, format_brightness, 3)

batwidget = wibox.widget.textbox()
vicious.cache(vicious.widgets.bat)
vicious.register(batwidget, vicious.widgets.bat, format_battery, 20, config.battery)

wifiwidget = wibox.widget.textbox()
vicious.cache(vicious.widgets.wifiiw)
vicious.register(wifiwidget, vicious.widgets.wifiiw, format_wifi, 3, config.net_interface)

cpuwidget = wibox.widget.textbox()
vicious.cache(vicious.widgets.cpu)
vicious.register(cpuwidget, vicious.widgets.cpu, format_cpu, 1)

memwidget = wibox.widget.textbox()
vicious.cache(vicious.widgets.mem)
vicious.register(memwidget, vicious.widgets.mem, format_memory, 1)

mpdwidget = wibox.widget.textbox()
mpdwidget.align = 'center'
vicious.cache(vicious.widgets.mpd)
vicious.register(mpdwidget, vicious.widgets.mpd, format_mpd)

thermalwidget = wibox.widget.textbox()
vicious.cache(vicious.widgets.thermal)
vicious.register(thermalwidget, vicious.widgets.thermal, format_thermal, 2, config.thermal_zone)

local widget_table = {
    ["mpd"] = mpdwidget,
    ["memory"] = memwidget,
    ["cpu"] = cpuwidget,
    ["volume"] = volumebox,
    ["backlight"] = backlightbox,
    ["wifi"] = wifiwidget,
    ["battery"] = batwidget,
    ["clock"] = mytextclock,
    ["thermal"] = thermalwidget,
}

Widgets = {}

function Widgets.get(conf)
    local t = {}

    for _, v in ipairs(conf) do
        table.insert(t, widget_table[v])
    end

    return t
end

return Widgets
