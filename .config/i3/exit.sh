#!/bin/sh

ANSWER=$(echo -e "suspend\nlock\nlogout\nshutdown\nreboot" | rofi -dmenu)

case $ANSWER in
	"suspend") systemctl suspend;;
	"lock") xset s activate;;
	"logout") i3-msg exit;;
	"shutdown") shutdown now;;
	"reboot") reboot;;
esac
