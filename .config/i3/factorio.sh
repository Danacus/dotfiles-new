#!/bin/sh

PACKPATH="$HOME/Games/Factorio/Modpacks/"
INSTANCE=$(ls $PACKPATH | rofi -dmenu -i)

echo $INSTANCE

if test -n "$INSTANCE"; then
    cd $(<"${PACKPATH}${INSTANCE}/pack.conf") && padsp  ./factorio --mod-directory ${PACKPATH}${INSTANCE}/ &
fi
