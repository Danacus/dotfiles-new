#!/bin/bash

INSTANCE=$(ls ~/.local/share/multimc/instances/ | rofi -dmenu -i)

echo $INSTANCE

if test -n "$INSTANCE"; then
	multimc -l $INSTANCE
fi
