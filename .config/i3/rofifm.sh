#!/bin/sh

RECENT_FILES=~/.config/i3/recent_files
FILE_MANAGER=dolphin
TERMINAL="termite -d"
DMENU="rofi -dmenu -i"

open_file() {
    sed -i "\:$1:d" $RECENT_FILES
    echo -e "$1\n$(cat $RECENT_FILES)" > $RECENT_FILES
    xdg-open "$1"
}

recent_files() {
    ITEM=$(cat $RECENT_FILES | sed 's!.*/!!' | $DMENU -format i)

    if [[ -z $ITEM ]]; then
        return
    fi

    ITEM=$(($ITEM+1))
    echo $ITEM

    open_file "$(sed -n "${ITEM}p" < $RECENT_FILES)"
}

DONE=false

if ! [[ -f $RECENT_FILES ]]; then
    touch $RECENT_FILES
fi

cd ~

ITEM=$({ echo "Recent Files"; echo "Open in terminal"; echo "Show in $FILE_MANAGER"; ls; } | $DMENU)

while [ $DONE != true ]; do

    if [[ -z $ITEM ]]; then
        break
    fi

    if [[ $ITEM = "Open in terminal" ]]; then
        $TERMINAL "$PWD"
        break
    elif [[ $ITEM = "Show in $FILE_MANAGER" ]]; then
        $FILE_MANAGER .
        break
    elif [[ $ITEM = "Recent Files" ]]; then
        recent_files
        break
    elif [[ -d $ITEM ]]; then
        cd "$ITEM"
        continue
    elif [[ -f $ITEM ]]; then
        open_file "$ITEM"
        break
    fi

    ITEM=$({ echo "Open in terminal"; echo "Show in $FILE_MANAGER"; ls; } | rofi -dmenu -i)

done



