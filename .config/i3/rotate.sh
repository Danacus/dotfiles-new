#!/bin/sh

LOG=/run/user/$(id -u $USER)/sensor.log
DNAME=eDP1

function rotate {
	#echo ---- rotete ----
	ORIENTATION=$1
	CUR_ROT=$(xrandr -q --verbose | grep $DNAME | cut -d" " -f6)

	NEW_ROT="normal"
	CTM="1 0 0 0 1 0 0 0 1"

	# Set the actions to be taken for each possible orientation
	case "$ORIENTATION" in
	normal)
		NEW_ROT="normal"
		CTM="1 0 0 0 1 0 0 0 1"
		;;
	bottom-up)
		NEW_ROT="inverted"
		CTM="-1 0 1 0 -1 1 0 0 1"
		;;
	right-up)
		NEW_ROT="right"
		CTM="0 1 0 -1 0 1 0 0 1"
		;;
	left-up)
		NEW_ROT="left"
		CTM="0 -1 1 1 0 0 0 0 1"
		;;
	esac

	xrandr -o $NEW_ROT
	xinput set-prop "Wacom HID 50DB Finger" --type=float "Coordinate Transformation Matrix" $CTM

}

rotate "$1"

# kill old monitor-sensor
#killall monitor-sensor

# Clear sensor.log so it doesn't get too long over time
#> $LOG

# Launch monitor-sensor and store the output in a variable that can be parsed by the rest of the script
#monitor-sensor >> $LOG 2>&1 &

# Parse output or monitor sensor to get the new orientation whenever the log file is updated
# Possibles are: normal, bottom-up, right-up, left-up
# Light data will be ignored
#while inotifywait -e modify $LOG; do
#	# Read the last line that was added to the file and get the orientation
#	ORIENTATION=$(tail -n 1 $LOG | grep 'orientation' | grep -oE '[^ ]+$')
#
#	if [ ! -z $ORIENTATION ] ; then
#		rotate $ORIENTATION
#	fi
#
#done
