#!/bin/sh

COMMAND=$(rofi -dmenu -i)

if test -n "$COMMAND"; then
    termite -e zsh -c "${COMMAND}"
fi
