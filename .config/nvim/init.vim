
set nocompatible              " be iMproved, required
filetype off                  " required

call plug#begin()
Plug 'vim-scripts/c.vim'
Plug 'arcticicestudio/nord-vim'
Plug 'scrooloose/nerdtree'
Plug 'jistr/vim-nerdtree-tabs'
Plug 'rhysd/vim-clang-format'
Plug 'kana/vim-operator-user'
Plug 'rust-lang/rust.vim'
"Plug 'Shougo/deoplete.nvim'
"Plug 'zchee/deoplete-clang'
"Plug 'Shougo/deoplete-clangx'
"Plug 'Shougo/neoinclude.vim'
"Plug 'roxma/nvim-yarp'
"Plug 'sebastianmarkow/deoplete-rust'
"Plug 'carlitux/deoplete-flow'
Plug 'jpalardy/vim-slime'
"Plug 'autozimu/LanguageClient-neovim', {
"            \ 'branch': 'next',
"            \ 'do': 'bash install.sh',
"            \ }
Plug 'calviken/vim-gdscript3'
"Plug 'Shougo/neosnippet'
"Plug 'Shougo/neosnippet-snippets'
Plug 'nikvdp/ejs-syntax'
Plug 'pangloss/vim-javascript'
Plug 'MaxMEllon/vim-jsx-pretty'
Plug 'jelera/vim-javascript-syntax'
Plug 'w0rp/ale'
Plug 'prettier/prettier'
Plug 'flrnprz/plastic.vim'
Plug 'rakr/vim-one'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'vim-pandoc/vim-rmarkdown'
Plug 'vim-pandoc/vim-pandoc'
Plug 'vim-pandoc/vim-pandoc-syntax'
Plug 'junegunn/fzf.vim'
Plug 'tpope/vim-dadbod'
"Plug 'lervag/vimtex'
Plug 'dpelle/vim-LanguageTool'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'tikhomirov/vim-glsl'
Plug 'soli/prolog-vim'
call plug#end()            " required

filetype plugin indent on    " required

autocmd FileType matlab setlocal keywordprg=info\ octave\ --vi-keys\ --index-search

augroup filetypedetect
  " Mail
  autocmd BufRead,BufNewFile *mutt-*              setfiletype mail
augroup END

let g:languagetool_jar = "/home/daan/.local/share/languagetool/languagetool-commandline.jar"

let g:jsx_ext_required = 0

let g:neosnippet#enable_completed_snippet = 1
let g:autocomplete_flow#insert_paren_after_function = 0

let g:languagetool_jar = "~/Documents/LanguageTool-4.9.1/languagetool-commandline.jar"

set tags+=./.tags

set nobackup
set nowritebackup

"Credit joshdick
"Use 24-bit (true-color) mode in Vim/Neovim when outside tmux.
"If you're using tmux version 2.2 or later, you can remove the outermost $TMUX check and use tmux's 24-bit color support
"(see < http://sunaku.github.io/tmux-24bit-color.html#usage > for more information.)
if (empty($TMUX))
  if (has("nvim"))
  "For Neovim 0.1.3 and 0.1.4 < https://github.com/neovim/neovim/pull/2198 >
  let $NVIM_TUI_ENABLE_TRUE_COLOR=1
  endif
  "For Neovim > 0.1.5 and Vim > patch 7.4.1799 < https://github.com/vim/vim/commit/61be73bb0f965a895bfb064ea3e55476ac175162 >
  "Based on Vim patch 7.4.1770 (`guicolors` option) < https://github.com/vim/vim/commit/8a633e3427b47286869aa4b96f2bfc1fe65b25cd >
  " < https://github.com/neovim/neovim/wiki/Following-HEAD#20160511 >
  if (has("termguicolors"))
    set termguicolors
  endif
endif

let g:airline_powerline_fonts = 1


" Session management

set switchbuf=useopen,usetab

let g:session = 0

fu! AutoSaveSess()
    if g:session == 1
        call SaveSess()
    endif
endfunction

fu! SaveSess()
    execute 'NERDTreeTabsClose'
    execute 'mksession! ' . getcwd() . '/.session.vim'
    let g:session = 1
endfunction

fu! RemoveSess()
    shell "rm " . getcwd() . "/.session.vim"
endfunction

fu! RestoreSess()
    if filereadable(getcwd() . '/.session.vim')
        let initial_args = argv()
        execute 'so ' . getcwd() . '/.session.vim'
        for file in initial_args
            if bufloaded(file) != 1
                execute 'tabnew ' . getcwd() . '/' . file 
            else
                execute 'sb ' . file
            endif
        endfor
        let g:session = 1
    endif
endfunction

:command Savesess call SaveSess()
:command Restoresess call RestoreSess()
:command Removesess call RemoveSess()

nnoremap <A-r> :Restoresess<cr>

" Save session on quitting Vim
autocmd VimLeave * call AutoSaveSess()

" Restore session on starting Vim
"autocmd VimEnter * nested call RestoreSess()

"----------------------

" move between tabs
nnoremap <A-h>  :tabprevious<CR>
nnoremap <A-l>  :tabnext<CR>


map <C-n> :NERDTreeMirrorToggle<CR>

" prevent mouse vanishing
set nomousehide

" toggles menu in graphical mode
function! ToggleGUICruft()
    if &guioptions=='ir'
        exec('set guioptions=imrLn')
    else
        exec('set guioptions=ir')
    endif
endfunction

map <F12> <Esc>:call ToggleGUICruft()<cr>

set number
set linebreak
set nobackup
set noswapfile
"set shortmess+=I
set backspace=indent,eol,start
set visualbell t_vb=
set showmode
"set smartcase

nnoremap <F1> <nop>
nnoremap Q <nop>
nnoremap K <nop>

runtime! ftplugin/man.vim
nnoremap K :Man <cword>

set mouse=a
set nu

let g:clang_format#code_style="llvm"

set clipboard=unnamed 
set backspace=indent,eol,start " allow backspacing over everything in insert mode

cmap w!! w !sudo tee > /dev/null %

set laststatus=2
let g:nord_uniform_diff_background = 1
let g:nord_cursor_line_number_background = 1
"let g:lightline = {
            "\ 'colorscheme': 'one',
            "\ }
let g:airline_theme='one'

set background=dark
colorscheme one

filetype plugin indent on
set smartindent
set tabstop=4
set shiftwidth=4
set expandtab

:set number relativenumber
:set nu rnu

:augroup numbertoggle
:  autocmd!
:  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
:  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
:augroup END

:command WQ wq
:command Wq wq
:command W w
:command Q q
:command WQa wqa
:command Wqa wqa

" Terminal stuff
:tnoremap <Esc> <C-\><C-n>
:set ma

let g:C_Mapfeader = ','
nnoremap <cr> :noh<CR><CR>:<backspace>

imap <C-k>     <Plug>(neosnippet_expand_or_jump)
smap <C-k>     <Plug>(neosnippet_expand_or_jump)
xmap <C-k>     <Plug>(neosnippet_expand_target)

imap <expr><TAB> neosnippet#expandable_or_jumpable() ?
\ "\<Plug>(neosnippet_expand_or_jump)"
\: pumvisible() ? "\<C-n>" : "\<TAB>"
smap <expr><TAB> neosnippet#expandable_or_jumpable() ?
\ "\<Plug>(neosnippet_expand_or_jump)"
\: "\<TAB>"

inoremap <silent><expr> <TAB>
      \ pumvisible() ? coc#_select_confirm() :
      \ coc#expandableOrJumpable() ? "\<C-r>=coc#rpc#request('doKeymap', ['snippets-expand-jump',''])\<CR>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

let g:coc_snippet_next = '<tab>'

nnoremap <C-f> :Files<cr>
nnoremap <C-b> :Buffers<cr>
nnoremap ,s :split<cr>
nnoremap .s :vsplit<cr>

nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" Disable arrow keys
map <Up> <Esc>
map <Down> <Esc>
map <Left> <Esc>
map <Right> <Esc>

imap <Up> <Esc>
imap <Down> <Esc>
imap <Left> <Esc>
imap <Right> <Esc>

set completeopt=menu,noinsert

