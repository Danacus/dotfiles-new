#!/usr/bin/env sh

killall polybar

if type "xrandr"; then
  for m in $(xrandr --query | grep " connected" | cut -d" " -f1); do
    MONITOR=$m polybar --reload bar1 &
    #MONITOR=$m polybar --reload touchbar &
  done
else
  polybar --reload bar1 &
  #polybar --reload touchbar &
fi

sleep 1

polybar-msg cmd hide
