/* vim:ft=css 
*/

/**
 * User: deadguy
 * Copyright: deadguy
 */

configuration {
	display-drun:    "Activate";
	display-run:     "Execute";
	display-window:  "Window";
	show-icons:      true;
	sidebar-mode:    true;
	columns: 1;
	separator-style: "solid";
	hide-scrollbar: true;
	fullscreen: false;
	color-normal: "#273238, #c1c1c1, #273238, #394249, #ffffff";
	color-urgent: "#273238, #ff1844, #273238, #394249, #ff1844";
	color-active: "#273238, #80cbc4, #273238, #394249, #80cbc4";
	color-window: "#273238, #273238, #1e2529";
}

* {
	background-color:            rgba(0,0,0,0.1);
	text-color:                  rgba(255, 255, 255, 0.5);
	selbg:                       #bcbcbc;
	actbg:                       rgba(0,0,0,0);
	urgbg:                       #e53935;
	winbg:			     #bcbcbc;

	selected-normal-foreground:  #ffffff;
	normal-foreground:           @text-color;
	selected-normal-background:  @actbg;
	normal-background:           rgba(0,0,0,0);

	selected-urgent-foreground:  @background-color;
	urgent-foreground:           @text-color;
	selected-urgent-background:  @urgbg;
	urgent-background:           @background-color;

	selected-active-foreground:  @winbg;
	active-foreground:           @text-color;
	selected-active-background:  @actbg;
	active-background:           @selbg;

	line-margin:                 2;
	line-padding:                2;
	separator-style:             "none";
	hide-scrollbar:              "true";
	margin:                      0;
	padding:                     0;
}

window {
	location:	 east;
	anchor:		 east;
	height:		 100%;
	width:		 15%;
	orientation: horizontal;
	children:	 [mainbox];
}

mainbox {
	spacing:  0em;
	children: [ entry,listview ];
}

button { padding: 5px 2px; }

button selected {
	background-color: @active-background;
	text-color:       @background-color;
}

inputbar {
	padding: 0px;
	spacing: 0px;
}

listview {
	spacing: 0.5em;
	dynamic: false;
	cycle:   true;
}

element { padding: 5px; }

entry {
	expand:         false;
	text-color:     @normal-foreground;
	vertical-align: 1;
	padding:        2px;
}

element normal.normal {
	background-color: @normal-background;
	text-color:       @normal-foreground;
}

element normal.urgent {
	background-color: @urgent-background;
	text-color:       @urgent-foreground;
}

element normal.active {
	background-color: @active-background;
	text-color:       @active-foreground;
}

element selected.normal {
	background-color: @selected-normal-background;
	text-color:       @selected-normal-foreground;
	border:           0 0 0 0;
	border-color:	    @active-background;
}

element selected.urgent {
	background-color: @selected-urgent-background;
	text-color:       @selected-urgent-foreground;
}

element selected.active {
	background-color: @selected-active-background;
	text-color:       @selected-active-foreground;
}

element alternate.normal {
	background-color: @normal-background;
	text-color:       @normal-foreground;
}

element alternate.urgent {
	background-color: @urgent-background;
	text-color:       @urgent-foreground;
}

element alternate.active {
	background-color: @active-background;
	text-color:       @active-foreground;
}
