# If you come from bash you might have to change your $PATH.
export PATH=$HOME/bin:/usr/local/bin:$PATH
export PATH="$PATH:$HOME/go/bin:$HOME/.cargo/bin:$HOME/.gem/ruby/2.7.0/bin:$HOME/.ghcup/bin"

export ZSH=/usr/share/oh-my-zsh

[[ -r "/usr/share/z/z.sh" ]] && source /usr/share/z/z.sh

# Path to your oh-my-zsh installation.

#POWERLEVEL9K_MODE='nerdfont-complete'
#
#POWERLEVEL9K_FOLDER_ICON=""
#
#POWERLEVEL9K_COMMAND_EXECUTION_TIME_THRESHOLD=0
#
#POWERLEVEL9K_DIR_OMIT_FIRST_CHARACTER=true
#
#POWERLEVEL9K_BACKGROUND_JOBS_FOREGROUND='black'
#POWERLEVEL9K_BACKGROUND_JOBS_BACKGROUND='178'
#POWERLEVEL9K_VCS_CLEAN_BACKGROUND='green'
#POWERLEVEL9K_VCS_UNTRACKED_BACKGROUND='yellow'
#POWERLEVEL9K_VCS_MODIFIED_BACKGROUND='016'
#POWERLEVEL9K_VIRTUALENV_BACKGROUND='green'
#POWERLEVEL9K_CONTEXT_DEFAULT_FOREGROUND="blue"
#POWERLEVEL9K_DIR_WRITABLE_FORBIDDEN_FOREGROUND="015"
#POWERLEVEL9K_NODE_VERSION_BACKGROUND="034"
#POWERLEVEL9K_JAVA_VERSION_BACKGROUND="016"
#POWERLEVEL9K_TIME_BACKGROUND='255'
#POWERLEVEL9K_COMMAND_EXECUTION_TIME_BACKGROUND='245'
#POWERLEVEL9K_COMMAND_EXECUTION_TIME_FOREGROUND='black'
#POWERLEVEL9K_SHORTEN_DIR_LENGTH=2
#
#POWERLEVEL9K_TIME_FORMAT="%D{%H:%M}"


#P9K__LEFT=(root_indicator dir dir_writable virtualenv vcs)
#P9K__RIGHT=(status background_jobs command_execution_time)
#
#function chpwd {
#    if [[ $PWD/ == ${HOME}/Documents/Development/Node/* ]]; then
#        POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=("${P9K__RIGHT[@]:0:2}" "node_version" "${P9K__RIGHT[@]:2}")
#    elif [[ $PWD/ == ${HOME}/Documents/Development/Java/* ]]; then
#        POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=("${P9K__RIGHT[@]:0:2}" "java_version" "${P9K__RIGHT[@]:2}")
#    elif [[ $PWD/ == ${HOME}/Documents/School/OGP/* ]]; then
#        POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=("${P9K__RIGHT[@]:0:2}" "java_version" "${P9K__RIGHT[@]:2}")
#    else
#    	POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(${P9K__RIGHT})
#    fi
#}

#POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(${P9K__LEFT})
#POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(${P9K__RIGHT})

#POWERLEVEL9K_SHOW_CHANGESET=true
#
if [[ $TERM == xterm-termite ]]; then
  . /etc/profile.d/vte.sh
  __vte_osc7
fi

HYPHEN_INSENSITIVE="true"
COMPLETION_WAITING_DOTS="true"

#source /usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme
ZSH_THEME="avit"

alias nodeindex="node lib/index.js"

plugins=(
  git
  archlinux
  tig gitfast colorize command-not-found cp dirhistory sudo
)

ZSH_COMPDUMP=/tmp/zcompdump-$USER
source $ZSH/oh-my-zsh.sh

HYPHEN_INSENSITIVE="true"
COMPLETION_WAITING_DOTS="true"

ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets pattern cursor)

# User configuration

export MANPATH="/usr/local/man:$MANPATH"
export LANG=en_US.UTF-8
export BROWSER=/usr/bin/firefox
export EDITOR=/usr/bin/nvim
export TERM="xterm-256color"
export LC_ALL=en_GB.utf8
export QT_QPA_PLATFORMTHEME="qt5ct"

if [[ -d $HOME/.emacs.d/bin && -z $(echo $PATH | grep -o $HOME/.emacs.d/bin) ]]
then
    export PATH="${PATH}:$HOME/.emacs.d/bin"
fi

# TP Stuff

declare -A beacons=(
    [doc]="${HOME}/Documents"
    [dev]="${HOME}/Docmuments/Development"
    [node]="${HOME}/Documents/Development/Node"
    [java]="${HOME}/Documents/Development/Java"
    [python]="${HOME}/Documents/Development/Python"
    [school]="${HOME}/Documents/School"
    [iw]="${HOME}/Documents/School/IW"
    [bvp]="${HOME}/Documents/School/BvP"
)

# tp() {
#    cd ${beacons[$1]}
# }


export FFF_CD_ON_EXIT=1
alias f="fff"


# NNN Stuff

export NNN_USE_EDITOR=1
export NNN_DE_FILE_MANAGER=dolphin

export NNN_TMPFILE="/tmp/nnn"
export NNN_PLUG="p:preview-tabbed"
export NNN_FIFO=/tmp/nnn.fifo

n () {
    # Block nesting of nnn in subshells
    if [ -n $NNNLVL ] && [ "${NNNLVL:-0}" -ge 1 ]; then
        echo "nnn is already running"
        return
    fi

    # The default behaviour is to cd on quit (nnn checks if NNN_TMPFILE is set)
    # To cd on quit only on ^G, remove the "export" as in:
    #     NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"
    # NOTE: NNN_TMPFILE is fixed, should not be modified
    export NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"

    # Unmask ^Q (, ^V etc.) (if required, see `stty -a`) to Quit nnn
    # stty start undef
    # stty stop undef
    # stty lwrap undef
    # stty lnext undef

    nnn "$@"

    if [ -f "$NNN_TMPFILE" ]; then
            . "$NNN_TMPFILE"
            rm -f "$NNN_TMPFILE" > /dev/null
    fi
}

nh() {
    NNN_SHOW_HIDDEN=1 n "$@"
}

export NNN_COPIER="echo -n $1 | xsel --clipboard --input"

# Python Virtualenv Stuff

export WORKON_HOME=~/Documents/Development/Python/virtualenvs
#source /usr/bin/virtualenvwrapper.sh

py() {
    PY_PREV_DIR=$PWD
# source "$1/bin/activate"  # commented out by conda initialize
}

depy() {
    deactivate
    cd $PY_PREV_DIR
}

alias mkpy="python -m venv"


bindkey -v
bindkey "^?" backward-delete-char
zle-line-init() { zle -K vicmd; }
zle -N zle-line-init

HYPHEN_INSENSITIVE="true"
COMPLETION_WAITING_DOTS="true"

# Aliases

# Zsh
alias zshrc="vim ~/.zshrc"
alias reload="source ~/.zshrc"

# Yay
alias syu="yay -Syu"
alias s="yay -S $@"
alias r="yay -R $@"

# Git

alias ga="git add ."
alias gc="git commit -m"
alias gac="git commit -a -m"
alias gpsh="git push"
alias gpll="git pull"

gacp() {
    gac $1 &&
    gpsh
}

# Configs

alias polybarc="$EDITOR ~/.config/polybar/config"
alias i3c="$EDITOR ~/.config/i3/config"
alias termitec="$EDITOR ~/.config/termite/config"
alias rofic="$EDITOR ~/.config/rofi/config.rasi"
alias vimrc="$EDITOR ~/.config/nvim/init.vim"

alias vim="nvim"
alias vi="vim"
alias v="vi"

alias sys="systemctl"
alias sysu="systemctl --user"

# Scripts

alias rotate="~/.config/i3/rotate.sh"

alias ant="JAVA_HOME=/usr/lib/jvm/java-11-openjdk ant"

source "tp"

alias ls=exa
alias l="exa -l"

alias c="alacritty --hold -e zsh & disown"

alias ds4="bluetoothctl power on && sudo ds4drv --hidraw --udp"

twitch() {
    $BROWSER "https://twitch.tv/popout/$1/chat" &
    mpv "https://twitch.tv/$1"
}

zoommode() {
    echo "Loading v4l2loopback module..."
    sudo modprobe v4l2loopback
    echo "Loading PulseAudio null-source..."
    pactl load-module module-null-source source_name=null
}

copy() {
    echo "${PWD}/${1}" | xclip -i
}

paste() {
    cp "$(xclip -o)" .
}

subfix() {
    for file in *.srt; do
        mv "$file" "$(basename "$file" .srt).srt.gz"
    done
    gzip -d *.srt.gz
}

wakezolder() {
    wol -p 8009 -i 192.168.1.200 d4:3d:7e:fc:0e:32
}

# /!\ do not use with zsh-autosuggestions

# /!\ zsh-syntax-highlighting and then zsh-autosuggestions must be at the end



#source /usr/share/nvm/init-nvm.sh
#source /usr/share/nvm/init-nvm.sh
# If you come from bash you might have to change your $PATH.
export PATH=$HOME/bin:/usr/local/bin:$PATH

#export ZSH=/usr/share/oh-my-zsh


eval $(thefuck --alias)
alias config='/usr/bin/git --git-dir=/home/daan/.cfg/ --work-tree=/home/daan'
alias config='/usr/bin/git --git-dir=/home/daan/.cfg/ --work-tree=/home/daan'
#source /home/daan/.local/bin/tp

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
#__conda_setup="$('/usr/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
#if [ $? -eq 0 ]; then
    #eval "$__conda_setup"
#else
    #if [ -f "/usr/etc/profile.d/conda.sh" ]; then
        #. "/usr/etc/profile.d/conda.sh"
    #else
        #export PATH="/usr/bin:$PATH"
    #fi
#fi
#unset __conda_setup
# <<< conda initialize <<<

