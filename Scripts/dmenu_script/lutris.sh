LUTRIS_DIR=$HOME/.local/share/lutris/

input () {
    sqlite3 "${LUTRIS_DIR}/pga.db" "select name from games"
}

output () {
    lutris lutris:rungame/$(sqlite3 "${LUTRIS_DIR}/pga.db" "select slug from games where name = '$1'")
}
